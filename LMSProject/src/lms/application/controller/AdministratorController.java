package lms.application.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javafx.scene.control.Alert.AlertType;
import lms.application.business.Author;
import lms.application.business.Book;
import lms.application.business.LendableCopy;
import lms.application.business.LibraryMember;
import lms.application.dataaccess.DataAccess;
import lms.application.dataaccess.DataAccessFacade;
import lms.application.main.Util;

public class AdministratorController extends BaseController {
	
	public List<Author> getAuthors(){
		DataAccess access = new DataAccessFacade();
		return access.getAuthors();
	}
	
	public List<Book> searchBook(String title, String isbn){
		
		DataAccess access = new DataAccessFacade();
		List<Book> books = access.getBooks();
		
		// get ALl
		if(title == "" && isbn ==""){
			return books;
		}
		
		//filterd
		List<Book> booksFilted = new ArrayList<Book>();
		for(Book book : books){
			if(book.getTitle().contains(title) || book.getIsbn().contains(isbn)){
				booksFilted.add(book);
			}
		}
		return booksFilted;
		
	}
	
	public List<LibraryMember> searchMember(String memberId, String lastName){
		
		DataAccess access = new DataAccessFacade();
		List<LibraryMember> members = access.getLibraryMembers();
		
		// get ALl
		if(memberId == "" && lastName ==""){
			return members;
		}
		//filterd
		List<LibraryMember> membersFilted = new ArrayList<LibraryMember>();
		for(LibraryMember member : members){
			if(member.getMemberId().contains(memberId) || member.getLastName().contains(lastName)){
				membersFilted.add(member);
			}
		}
		return membersFilted;
		
	}
	
	public void createBook(Book book){
		if(book.getTitle() == null || book.getTitle() == "" || book.getTitle().isEmpty()){
			this.displayMessage(AlertType.ERROR, "Title is required", "Alert Message");
			return;
		}
		if(book.getIsbn() == null || book.getIsbn() == "" || book.getIsbn().isEmpty()){
			this.displayMessage(AlertType.ERROR, "Isbn is required", "Alert Message");
			return;
		}
		if(book.getMaxDaysLoan() <= 0){
			this.displayMessage(AlertType.ERROR, "Max days checkout lenght is required", "Alert Message");
			return;
		}
		if(book.getAuthors() == null || book.getAuthors().size() <=0){
			this.displayMessage(AlertType.ERROR, "Author(s) is required", "Alert Message");
			return;
		}
		
		DataAccess access = new DataAccessFacade();
		boolean rs = access.createBook(book);
		if(!rs){
			this.displayMessage(AlertType.ERROR, "Exception from DB layer", "Alert Message");
			return;
		}
		this.displayMessage(AlertType.INFORMATION, "Book has been added successfully", "Successfully");
	}
	

	//tam bookLookup method
	public boolean bookLookUp(String isbn){
		
		DataAccess dac = new DataAccessFacade();
		List<Book> books = dac.getBooks();
		
		if(books != null){
		
			for (Book book : books) {		
	
				if (isbn.equalsIgnoreCase(book.getIsbn())) {
					return true;
				}
			}
		}
		return false;		
	}
	
	//tam getBookByISBN method
	public Book getBookByISBN(String isbn){
		
		DataAccess dac = new DataAccessFacade();
		List<Book> books = dac.getBooks();
		
		if(books != null){
		
			for (Book book : books) {		
	
				if (isbn.equalsIgnoreCase(book.getIsbn())) {
					return book;
				}
			}
		}
		return null;		
	}
		
	
	//tam
	private int getCopyId(){
		
		//generate copyid
		Random rand = new Random();
		return rand.nextInt(100) + 1;
		
	}
	
	//tam addBookCopy method
	public boolean addBookCopy(String isbn){
		
		//get book by isbn
		Book book = getBookByISBN(isbn);
		int copyId = getCopyId();
		
		//set lendable copy's publication and copyId
		//NB- book subclass of publicatio
		LendableCopy copy = new LendableCopy();
		copy.setPublication(book);
		copy.setCopyId(copyId);
		
		//save to data server
		DataAccess dac = new DataAccessFacade();
		dac.saveLendableBookCopy(copy );		
		
		return true;
	}
	

	public void addMember(LibraryMember member)
	{
		//data validation
		boolean rs = this.validateMemberInfo(member);
		if(rs){
			DataAccess da=new DataAccessFacade();
			da.saveLibraryMember(member);
			this.displayMessage(AlertType.INFORMATION, "Memeber is registered successfully", "Saved");
		}
	}
	private boolean validateMemberInfo(LibraryMember member){
		//data validation
		if(member.getMemberId().isEmpty())
		{
			this.displayMessage(AlertType.ERROR, "Please enter memeber id", "Empty");
			return false;
		}
		if(member.getFirstName().isEmpty())
		{
			this.displayMessage(AlertType.ERROR, "Please enter First Name", "Empty");
			return false;
		}
		if(member.getLastName().isEmpty())
		{
			this.displayMessage(AlertType.ERROR, "Please enter last name", "Empty");
			return false;
		}
		if(member.getPhone().isEmpty())
		{
			this.displayMessage(AlertType.ERROR, "Please enter phone", "Empty");
			return false;
		}
		if(!Util.validatePhone(member.getPhone()))
		{
			this.displayMessage(AlertType.ERROR, "Phone number is not correct USA format", "Invalid Phone");
			return false;
		}
		if(member.getStreet().isEmpty())
		{
			this.displayMessage(AlertType.ERROR, "Please enter street", "Empty");
			return false;
		}
		if(member.getCity().isEmpty())
		{
			this.displayMessage(AlertType.ERROR, "Please enter city", "Empty");
			return false;
		}
		if(member.getZipCode().isEmpty())
		{
			this.displayMessage(AlertType.ERROR, "Please enter zipcode", "Empty");
			return false;
		}
		return true;
	}
	public void updateLibraryMember(LibraryMember member)
	{
		//data validation
		boolean rs = this.validateMemberInfo(member);
		if(rs){
			DataAccess da=new DataAccessFacade();
			da.updateLibraryMember(member);
			this.displayMessage(AlertType.INFORMATION, "Memeber is updated successfully", "Updated");
		}
	}
	public void deleteMember(String memberId)
	{
		DataAccess da=new DataAccessFacade();
		da.deleteLibraryMember(memberId);
	}
	
	public LibraryMember getLibraryMemberById(String memberId){
		
		if(memberId == null || memberId == "" || memberId.isEmpty())
		{
			this.displayMessage(AlertType.ERROR, "Please enter memeber id", "Empty");
			return null;
		}
		
		DataAccess da=new DataAccessFacade();
		List<LibraryMember> members = da.getLibraryMembers();
		for(LibraryMember member : members){
			if(memberId.equalsIgnoreCase(member.getMemberId())){
				return member;
			}
		}
		return null;
	}

}
