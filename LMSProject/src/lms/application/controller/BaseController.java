package lms.application.controller;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import lms.application.business.User;

public class BaseController {

	private String member;
	public User contextUser;

	public String getMember() {
		return member;
	}

	public void setMember(String member) {
		this.member = member;
	}
	
	public void displayMessage(AlertType type, String msg, String title){
		Alert alert = new Alert(type);
		alert.setTitle(title);
		alert.setHeaderText(null);
		alert.setContentText(msg);
		alert.showAndWait();
	}
}
