package lms.application.controller;

import java.util.ArrayList;
import java.util.List;

import javafx.print.Printer;
import javafx.print.PrinterJob;
import javafx.scene.Node;
import javafx.scene.control.Alert.AlertType;
import lms.application.business.Book;
import lms.application.business.CheckoutRecord;
import lms.application.business.LendableCopy;
import lms.application.dataaccess.DataAccess;
import lms.application.dataaccess.DataAccessFacade;

public class LibrarianController extends BaseController {
	
	public Book isBookAvailableByISBN(String ISBN){
		
		DataAccess access = new DataAccessFacade();
		List<Book> lst = access.getBooks();
		if(lst != null){
		for(int i=0; i< lst.size(); i++){
			if(ISBN.equalsIgnoreCase(lst.get(i).getIsbn())){
				// isbn match, check copies
					List<LendableCopy> copies = access.getLendableCopies();
					for(int j=0; j< copies.size(); j++){
						if(((Book)copies.get(j).getPublication()).getIsbn().equalsIgnoreCase(lst.get(i).getIsbn()))
							return lst.get(i);
					}
				}
			}
		}
		return null;
		
	}
	
			
	public List<String> getCheckoutsByMemberId(String MemberId){
		DataAccess access = new DataAccessFacade();
		List<CheckoutRecord> lst = access.getCheckoutRecords();
		
		if(lst == null || lst.isEmpty()){
			this.displayMessage(AlertType.ERROR, "No checkout records were found in system at all.", "No Results");
			return null;
		}
		
		List<String> results = new ArrayList<String>();
		for(int i=0; i< lst.size(); i++){
			if(lst.get(i).getMemberId() == MemberId){
				results.add(lst.get(i).getTitle());
			}
		}
		
		if(results.isEmpty())
			this.displayMessage(AlertType.ERROR, "No matching records were found for specified member ID.", "No Results");

		return results;
	}
	
	public void print(Node document){
		Printer printer = Printer.getDefaultPrinter();
		PrinterJob job = PrinterJob.createPrinterJob(printer);
		if(job == null)
	    	this.displayMessage(AlertType.INFORMATION, "Printer is not available or not working", "Alert Message");
		
		if(job.showPageSetupDialog(null))
			this.displayMessage(AlertType.INFORMATION, "Printer is not available or not working", "Alert Message");
		
		if(!job.printPage(document))
			this.displayMessage(AlertType.INFORMATION, "Printer is not available or not working", "Alert Message");
					
	}
	
	public List<LendableCopy> getLendableCopyByIsbn(String isbn){
		DataAccess access = new DataAccessFacade();
		List<LendableCopy> copyFilted = new ArrayList<LendableCopy>();
		List<LendableCopy> copies = access.getLendableCopies();
		for(int j=0; j< copies.size(); j++){
			if(((Book)copies.get(j).getPublication()).getIsbn().equalsIgnoreCase(isbn))
				copyFilted.add(copies.get(j));
		}
		return copyFilted;
	}
}
