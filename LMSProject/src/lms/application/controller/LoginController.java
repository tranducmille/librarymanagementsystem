package lms.application.controller;

import javafx.scene.control.Alert.AlertType;
import lms.application.dataaccess.DataAccessFacade;

public class LoginController extends BaseController{

	public boolean login(String userId, String password){
      //validation
      if(userId == null || userId.isEmpty() ){
    	  this.displayMessage(AlertType.ERROR, "UserId is required", "Alert");
    	  return false;
      }
      if(password == null|| password.isEmpty()){
    	  this.displayMessage(AlertType.ERROR, "Password is required", "Alert");
    	  return  false;
      }
      
		DataAccessFacade facade = new DataAccessFacade();
		boolean rs = facade.login(Integer.parseInt(userId), password);
		if(!rs){
			this.displayMessage(AlertType.INFORMATION, "UserId or password is not correct", "Information");
			 return  false;
		}
		return rs;
	}
}
