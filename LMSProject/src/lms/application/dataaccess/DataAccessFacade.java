package lms.application.dataaccess;
import static java.nio.file.StandardOpenOption.*;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import lms.application.business.Author;
import lms.application.business.Book;
import lms.application.business.CheckoutRecord;
import lms.application.business.LendableCopy;
import lms.application.business.LibraryMember;
import lms.application.business.User;

public class DataAccessFacade implements DataAccess {
	public static final String OUTPUT_DIR =// System.getProperty("user.dir") 
			"C:\\temp";
	public static final String DATE_PATTERN = "MM/dd/yyyy";
	
	public void saveLibraryMember(LibraryMember member) {
		ObjectOutputStream out = null;
		try {
			
			List<LibraryMember> members = this.getLibraryMembers();
			if(members == null){
				members = new ArrayList<LibraryMember>();
			}
			members.add(member);
			Path path = FileSystems.getDefault().getPath(OUTPUT_DIR, "LibraryMember");
			out = new ObjectOutputStream(Files.newOutputStream(path));
			out.writeObject(members);
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			if(out != null) {
				try {
					out.close();
				} catch(Exception e) {}
			}
		}
	}
	
	public List<LibraryMember> getLibraryMembers(){
		ObjectInputStream in = null;
		List<LibraryMember> members = null;
		try {
			Path path = FileSystems.getDefault().getPath(OUTPUT_DIR, "LibraryMember");
			in = new ObjectInputStream(Files.newInputStream(path));
			members = (List<LibraryMember>)in.readObject();
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if(in != null) {
				try {
					in.close();
				} catch(Exception e) {}
			}
		}
		return members;
	}
	public LibraryMember readLibraryMember(String name) {
		ObjectInputStream in = null;
		LibraryMember member = null;
		try {
			Path path = FileSystems.getDefault().getPath(OUTPUT_DIR, name);
			in = new ObjectInputStream(Files.newInputStream(path));
			member = (LibraryMember)in.readObject();
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if(in != null) {
				try {
					in.close();
				} catch(Exception e) {}
			}
		}
		return member;
	}
	

	
	@Override
	public boolean login(int userId, String password) {
		ObjectInputStream in = null;
		List<User> users = null;
		try {
			Path path = FileSystems.getDefault().getPath(OUTPUT_DIR, "User");
			in = new ObjectInputStream(Files.newInputStream(path));
			users = (List<User>)in.readObject();
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if(in != null) {
				try {
					in.close();
				} catch(Exception e) {}
			}
		}
		
		User loginUser = new User(userId, password, null, null, null, null, null, null, null);
		for(User user : users){
			if(user.equals(loginUser)){
				return true;
			}
		}
		return false;
	}
	

	@Override
	public List<Author> getAuthors() {
	
		ObjectInputStream in = null;
		List<Author> authors = null;
		try {
			Path path = FileSystems.getDefault().getPath(OUTPUT_DIR, "Author");
			in = new ObjectInputStream(Files.newInputStream(path));
			authors = (List<Author>)in.readObject();
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if(in != null) {
				try {
					in.close();
				} catch(Exception e) {}
			}
		}
		return authors;
	}
	
	@Override
	public List<CheckoutRecord> getCheckoutRecords() {
		// TODO Auto-generated method stub
		ObjectInputStream in = null;
		List<CheckoutRecord> checkouts = null;
		try {
			Path path = Paths.get(OUTPUT_DIR + "\\Checkout");
			File f = new File(OUTPUT_DIR + "\\Checkout");
			if(!f.exists()){ 
				f.createNewFile();				
			}			
			in = new ObjectInputStream(Files.newInputStream(path ));
			checkouts = (List<CheckoutRecord>)in.readObject();
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if(in != null) {
				try {
					in.close();
				} catch(Exception e) {}
			}
		}
		return checkouts;
	}
	
	public List<Book> getBooks(){
		ObjectInputStream in = null;
		List<Book> books = null;
		try {
			Path path = FileSystems.getDefault().getPath(OUTPUT_DIR, "Book");
			in = new ObjectInputStream(Files.newInputStream(path));
			books = (List<Book>)in.readObject();
		} catch(Exception e) {
			//e.printStackTrace();
		} finally {
			if(in != null) {
				try {
					in.close();
				} catch(Exception e) {}
			}
		}
		return books;
	}
	
		
	
	@Override
	public boolean createBook(Book book) {
		ObjectOutputStream out = null;
		try {
			
			List<Book> books = this.getBooks();
			if(books == null){
				books = new ArrayList<Book>();
			}
			books.add(book);
			Path path = FileSystems.getDefault().getPath(OUTPUT_DIR, "Book");
			out = new ObjectOutputStream(Files.newOutputStream(path));
			out.writeObject(books);
		} catch(IOException e) {
			e.printStackTrace();
			return false;
		} finally {
			if(out != null) {
				try {
					out.close();
				} catch(Exception e) {}
			}
		}
		return true;
	}
	
	public void updateLibraryMember(LibraryMember member)
	{
		try {
			Path path = FileSystems.getDefault().getPath(OUTPUT_DIR, member.getMemberId());
		    Files.delete(path);
			} 
		catch (NoSuchFileException x) {
		  // this.displayMessage(Alert.AlertType.INFORMATION, "You have no member with this Id, try again", "Canceling...");;
		}  catch (IOException x) {
		   
		}
		//saveLibraryMember(member.getMemberId(), member);
		//this.displayMessage(AlertType.INFORMATION, "Memeber information updated successfully", "Updated");
	}
	public void deleteLibraryMember(String memberId)
	{
		try {
			Path path = FileSystems.getDefault().getPath(OUTPUT_DIR, memberId);
		    Files.delete(path);
		   // this.displayMessage(AlertType.INFORMATION, "Member with member id "+memberId+" is deleted", "Deleted");
		} catch (NoSuchFileException x) {
		   //this.displayMessage(Alert.AlertType.INFORMATION, "You have no member with this Id, try again", "Deleting Member");;
		}  catch (IOException x) {
		   
		}
		
	}
	
	//save lendable copy by tam	
	@Override
	public boolean saveLendableBookCopy(LendableCopy copy ) {
		ObjectOutputStream out = null;
		try {
			//shadi: modified to append copy to the list of copies
			List<LendableCopy> copies = this.getLendableCopies();
			if(copies == null){
				copies= new ArrayList<LendableCopy>();
			}
			copies.add(copy);
			Path path = FileSystems.getDefault().getPath(OUTPUT_DIR, "LendableCopy");
			out = new ObjectOutputStream(Files.newOutputStream(path));			
			out.writeObject(copies);
		} catch(IOException e) {
			e.printStackTrace();
			return false;
		} finally {
			if(out != null) {
				try {
					out.close();
				} catch(Exception e) {}
			}
		}
		return true;
	}
	
	
	// shadi: get lendable copies
	@Override
	public List<LendableCopy> getLendableCopies() {
		ObjectInputStream in = null;
		List<LendableCopy> copies = null;
		try {
			Path path = FileSystems.getDefault().getPath(OUTPUT_DIR, "LendableCopy");
			in = new ObjectInputStream(Files.newInputStream(path));
			copies = (List<LendableCopy>)in.readObject();
		} catch(Exception e) {
			//e.printStackTrace();
		} finally {
			if(in != null) {
				try {
					in.close();
				} catch(Exception e) {}
			}
		}
		return copies;
		/*// TODO Auto-generated method stub
		ObjectInputStream in = null;
		List<LendableCopy> copies = null;
		try {
			Path path = Paths.get(OUTPUT_DIR + "\\LendableCopy");
			File f = new File(OUTPUT_DIR + "\\LendableCopy");
			if(!f.exists()){ 
				f.createNewFile();				
			}			
			in = new ObjectInputStream(Files.newInputStream(path ));
			copies = (List<LendableCopy>)in.readObject();
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if(in != null) {
				try {
					in.close();
				} catch(Exception e) {}
			}
		}
		return copies;*/
	}
	
}
