package lms.application.dataaccess;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import lms.application.business.Author;
import lms.application.business.Book;
import lms.application.business.LendableCopy;
import lms.application.business.LibraryMember;
import lms.application.business.User;

public class initData {
	
	public static final String OUTPUT_DIR =// System.getProperty("user.dir") 
			"C:\\temp";
	public static final String DATE_PATTERN = "MM/dd/yyyy";
	
	public static void saveAuthors(String name, List<Author> authors) {
		ObjectOutputStream out = null;
		try {
			Path path = FileSystems.getDefault().getPath(OUTPUT_DIR, name);
			out = new ObjectOutputStream(Files.newOutputStream(path));
			out.writeObject(authors);
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			if(out != null) {
				try {
					out.close();
				} catch(Exception e) {}
			}
		}
	}
	
	public static void saveBooks(String name, List<Book> books) {
		ObjectOutputStream out = null;
		try {
			Path path = FileSystems.getDefault().getPath(OUTPUT_DIR, name);
			out = new ObjectOutputStream(Files.newOutputStream(path));
			out.writeObject(books);
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			if(out != null) {
				try {
					out.close();
				} catch(Exception e) {}
			}
		}
	}
	public static void saveLibraryMember(String name, List<LibraryMember> members) {
		ObjectOutputStream out = null;
		try {
			Path path = FileSystems.getDefault().getPath(OUTPUT_DIR, name);
			out = new ObjectOutputStream(Files.newOutputStream(path));
			out.writeObject(members);
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			if(out != null) {
				try {
					out.close();
				} catch(Exception e) {}
			}
		}
	}
	
	public static void saveUser(String name, List<User> users) {
		ObjectOutputStream out = null;
		try {
			Path path = FileSystems.getDefault().getPath(OUTPUT_DIR, name);
			out = new ObjectOutputStream(Files.newOutputStream(path));
			out.writeObject(users);
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			if(out != null) {
				try {
					out.close();
				} catch(Exception e) {}
			}
		}
	}
	
	public static void main(String arg[]){
		
		List<Author> authors = new ArrayList<Author>();
		authors.add(new Author("credentials value", "Zadok was born in South Africa in 1972 to a South African mother and an Israeli father and grew up in Kensington ", 1, "Rachel", "Zadok", "(800) 960-4166", "Sims Ave", "Akansas City", "Akansas", "53456"));
		authors.add(new Author("credentials value", "He is an American writer of science fiction and fantasy. He is known for the Thrawn series of Star Wars novels", 2, "Timothy", "Zahn", "(800) 876-9763", "Carpenter Ave", "Fairfield City", "Iowa", "52557"));
		authors.add(new Author("credentials value", "She is a fiction writer, columnist, editor, publisher, and former television and radio show host", 3, "Jessica", "Zafra", "(800) 513-8297", "City Hall Ave", "Washington City", "Washington", "45334"));
		authors.add(new Author("credentials value", "Robert Zend (1929 - 1985) was a Hungarian-Canadian poet, fiction writer, and multi-media artist.", 4, "Robert", "Zend", "(800) 746-0461", "Burlington Ave", "Fairfield City", "Iowa", "52557"));
		authors.add(new Author("credentials value", "Richard Zimler (born 1 January 1956 in Roslyn Heights, New York) is a best-selling author of fiction", 5, "Richard", "Zimler", "(800) 798-7913", "Hilltons Ave", "Michigan City", "Michigan", "43434"));
		authors.add(new Author("credentials value", "Markus Frank Zusak, (born 23 June 1975) is an Australian writer. He is best known for The Book Thief and The Messenger ", 6, "Markus", "Zusak", "(800) 632-3918", "4th street", "Fairfield City", "Iowa", "52557"));
		saveAuthors("Author",authors );
		
		saveBooks("Book",null);
		saveLibraryMember("LibraryMember",null);
		
		List<User> users = new ArrayList<User>();
		users.add(new User(123,"duc",  null, null, null, null, null, null, null));
		users.add(new User(234,"tam",  null, null, null, null, null, null, null));
		users.add(new User(456,"saddi",  null, null, null, null, null, null, null));
		users.add(new User(567,"usama", null, null, null, null, null, null, null));
		saveUser("User",users );
		
		System.out.println("Database has been executed");
	}
}
