package lms.application.dataaccess;

import java.util.List;

import lms.application.business.Author;
import lms.application.business.Book;
import lms.application.business.CheckoutRecord;
import lms.application.business.LendableCopy;
import lms.application.business.LibraryMember;

public interface DataAccess {
	public void saveLibraryMember(LibraryMember member);
	public LibraryMember readLibraryMember(String name);
	public boolean login(int userId, String password);
	public List<Author> getAuthors();
	public List<LibraryMember> getLibraryMembers();

	//shadi: checkoutrecords
	public List<CheckoutRecord> getCheckoutRecords();
	public List<LendableCopy> getLendableCopies();
	
	public boolean createBook(Book book);

	public List<Book> getBooks();

	
	public void deleteLibraryMember(String memberId);
	public void updateLibraryMember(LibraryMember member);
	boolean saveLendableBookCopy(LendableCopy copy );

}
