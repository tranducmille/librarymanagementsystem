package lms.application.business;

public class Author extends Person {

	private static final long serialVersionUID = 1L;
	private String credentials;
	private String shortBio;
	
	public Author(String credentials, String shortBio,int id, String firstName, String lastName, String phoneNo, String streetName, String city,
			String state, String zip) {
		super(id,firstName,lastName,phoneNo,streetName,city,state,zip);
		this.credentials = credentials;
		this.shortBio = shortBio;
	}

	public String getCredentials() {
		return credentials;
	}

	public String getShortBio() {
		return shortBio;
	}
}
