package lms.application.business;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

abstract public class Publication implements Serializable {
	
	private static final long serialVersionUID = 2010893663327964921L;
	private List<LendableCopy> lendableCopy;//Modified by tam
	private LocalDate dateDue;
	private String title;
	private int maxDaysLoan;
	
	public void setTitle(String title) {
		this.title = title;
	}
	public void setMaxDaysLoan(int maxDaysLoan) {
		this.maxDaysLoan = maxDaysLoan;
	}
	protected void setDateDue(LocalDate d) {
		dateDue = d;
	}
	public Publication(String title) {
		this.title = title;
	}
	public LocalDate getDateDue() {
		return dateDue;
	}
	public String getTitle() {
		return title;
	}
	public int getMaxDaysLoan() {
		return maxDaysLoan;
	}
	
	//tam
	public void addLendableCopy(LendableCopy lendableCopy) {
		
		this.lendableCopy.add(lendableCopy);
		
	}
	
	
}
