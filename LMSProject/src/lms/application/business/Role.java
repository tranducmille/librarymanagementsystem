package lms.application.business;

import java.util.ArrayList;
import java.util.List;

public class Role {
	private int roleId;
	private String roleName;
	
	public Role(){}
	public Role(int roleId, String roleName){
		this.roleId = roleId;
		this.roleName = roleName;
	}

	public List<Role> getRoles(){
		List<Role> roles = new ArrayList<Role>();
		Role role = new Role(1,"Librarian");
		Role role1 = new Role(2,"Administrator");
		Role role2 = new Role(3,"BOTH");
		roles.add(role);
		roles.add(role1);
		roles.add(role2);
		return roles;
	}
}
