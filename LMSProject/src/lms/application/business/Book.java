package lms.application.business;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Book extends Publication implements Serializable {
	private int id;
	private String isbn;
	private boolean available;
	private List<Author> authors = new ArrayList<Author>();
	public Book(int id, String isbn, String title, LocalDate dueDate, int maxDaysLoan,List<Author> authors, boolean isAvailable ) {
		super(title);
		this.setDateDue(dueDate);
		this.setTitle(title);
		this.setMaxDaysLoan(maxDaysLoan);
		this.setId(id);
		this.setIsbn(isbn);
		this.authors = authors;
		this.isAvailable(isAvailable);
	}
	public boolean isAvailable() {
		return available;
	}
	public List<Author> getAuthors() {
		return authors;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public void isAvailable(boolean b) {
		available = b;
	}
	@Override
	public String toString() {
		return "id: " + id + ", isbn: " + isbn + ", available: " + available;
	}
}
