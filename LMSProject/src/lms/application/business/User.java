package lms.application.business;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class User extends Person implements Serializable {
	private String password;
	private List<Role> roles = new ArrayList<Role>();
	
	public User(int userId,String password,String firstName, String lastName, String phoneNo, String streetName, String city,String state,String zip){
		super(userId,firstName,lastName,phoneNo,streetName,city,state,zip);
		this.setPassword(password);
	}
	
	public List<Role> getRoles() {
		return new Role().getRoles();
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (password == null || this.getId() == 0) {
				return false;
		} else if (!password.equals(other.password) || this.getId() != other.getId())
			return false;
		return true;
	}
}
