package lms.application.business;
import java.io.Serializable;

public class LendableCopy implements Serializable {
	private Publication publication;
	private int copyId; 
	public void setPublication(Publication publication) {
		this.publication = publication;
	}
	
	//shadi: return publication to match with book lookup
	public Publication getPublication(){
		return this.publication;
	}
	
	public void setCopyId(int copyId) {
		this.copyId = copyId;
	}
	public String toString() {
		return publication.toString();
	}
	
}
