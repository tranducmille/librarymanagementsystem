package lms.application.business;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CheckoutRecord implements Serializable {
	
	private static final long serialVersionUID = -3119855589946373695L;
	private LibraryMember member;
	private List<CheckoutRecordEntry> entries = new ArrayList<>();
	public void addEntry(CheckoutRecordEntry c) {
		entries.add(c);
	}
	
	public String getTitle(){
		return "Name: " + member.getFirstName() + " " + member.getLastName()  + " - Entries:" + entries.size() ;
	}
	
	public String getMemberId(){
		return member.getMemberId();
	}
	
	public String toString() {
		return entries.toString();
	}
}
