package guiForm;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import lms.application.business.Author;
import lms.application.business.Book;
import lms.application.controller.AdministratorController;

public class AddBookForm  extends Stage implements Initializable{
	@FXML
	private TextField bookTitle;
	@FXML
	private TextField isbn;
	@FXML
	private CheckBox availabilityId;
	@FXML
	private TextField maxCheckoutLength;
	@FXML
	private TableView<Author> authorsList;
	
	private AdministratorController adminController;
	
	public void setStage(){
		Parent root = null;
		try {
			root = FXMLLoader.load(getClass().getResource("AddBook.fxml"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Scene scene = new Scene(root,576, 431);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		initModality(Modality.WINDOW_MODAL);
		setScene(scene);
	}
	
	// Event Listener on Button.onAction
	@FXML
	public void handleAddAction(ActionEvent event) {
		List<Author> selectedAuthor = new ArrayList<Author>();
		for(Author author : authorsList.getSelectionModel().getSelectedItems()){
			selectedAuthor.add(author);
		}
		int maxDaysLoan = maxCheckoutLength.getText() == "" || maxCheckoutLength.getText().isEmpty() ? 0 : Integer.parseInt(maxCheckoutLength.getText());
		Book book = new Book(0, isbn.getText(), bookTitle.getText(), LocalDate.now(),maxDaysLoan ,
				selectedAuthor, availabilityId.isSelected());
		adminController.createBook(book);
	}
	// Event Listener on Button.onAction
	@FXML
	public void handleClearAction(ActionEvent event) {
		this.bookTitle.setText("");
		this.isbn.setText("");
		this.maxCheckoutLength.setText("");
		this.availabilityId.setSelected(true);
		authorsList.getSelectionModel().clearSelection();
	}
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		adminController = new AdministratorController();
		ObservableList<Author> items = FXCollections.observableArrayList();
		items.addAll(adminController.getAuthors());
		
		authorsList.setItems(items);
		ObservableList<TablePosition> selectedCells = FXCollections.observableArrayList();
		TableColumn<Author,String> firstCol = new TableColumn<>("First Name");
		firstCol.setCellValueFactory(new PropertyValueFactory<>("firstName"));

        TableColumn<Author, String> lastNameCol = new TableColumn<>("Last Name");
        lastNameCol.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        
        TableColumn<Author, String> phoneNoCol = new TableColumn<>("Phone No");
        phoneNoCol.setCellValueFactory(new PropertyValueFactory<>("phoneNo"));
        
        TableColumn<Author, String> streetCol = new TableColumn<>("Street Name");
        streetCol.setCellValueFactory(new PropertyValueFactory<>("streetName"));
        
        TableColumn<Author, String> shortBioCol = new TableColumn<>("Short Bio");
        shortBioCol.setCellValueFactory(new PropertyValueFactory<>("shortBio"));

        authorsList.getColumns().addAll(firstCol,lastNameCol,phoneNoCol, streetCol, shortBioCol);
        authorsList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        authorsList.getSelectionModel().setCellSelectionEnabled(false);
        //maybe you want onTouchPressed here for tablet
        authorsList.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                for (TablePosition tp : selectedCells){
                	authorsList.getSelectionModel().select(tp.getRow(), tp.getTableColumn());
                }
            }
        });
        
        maxCheckoutLength.lengthProperty().addListener(new ChangeListener<Number>(){
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) { 
                  if(newValue.intValue() > oldValue.intValue()){
                      char ch = maxCheckoutLength.getText().charAt(oldValue.intValue());
                      if(!(ch >= '0' && ch <= '9' )){    
                    	  maxCheckoutLength.setText(maxCheckoutLength.getText().substring(0,maxCheckoutLength.getText().length()-1)); 
                      }
                 }
            }
       });

	}
}
