package guiForm;

import javafx.stage.Stage;

public class BaseForm {
	private Stage primaryState;

	public void setPrimaryState(Stage primaryState) {
		this.primaryState = primaryState;
	}

	public Stage getPrimaryState() {
		return primaryState;
	}
}
