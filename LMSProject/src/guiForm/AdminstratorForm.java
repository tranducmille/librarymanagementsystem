package guiForm;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class AdminstratorForm extends Stage{

	@FXML
	private Button addMemberId;
	@FXML
	private Button addBookId;
	@FXML
	private Button addBookCopyId;
	@FXML
	private MenuItem manageMember;
	@FXML
	private MenuItem manageBook;
	
	@FXML private AnchorPane ap;

	public void setStage(){
		AnchorPane anchor = null;
		try {
			anchor = FXMLLoader.load(getClass().getResource("AdminstratorForm.fxml"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Scene scene = new Scene(anchor,895, 342);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		setScene(scene);
	}
	
	// Event Listener on Button.onAction
	@FXML
	public void handleAddMemberAction(ActionEvent event) {
		MemberForm memberForm = new MemberForm();
		memberForm.setStage();
		memberForm.show();
	}
	
	// Event Listener on Button.onAction
	@FXML
	public void handleAddBookAction(ActionEvent event) {
		AddBookForm addbook = new AddBookForm();		
		addbook.setStage();		
		addbook.show();		
	}
	
	@FXML
	public void handleCheckoutBtn(ActionEvent event) {
		LibrarianForm lbf = new LibrarianForm();
		lbf.setStage();
		lbf.show();		
	}
	
	// Event Listener on Button.onAction
	@FXML
	public void handleAddBookCopyAction(ActionEvent event) {
		AddCopyController addbookcopy = new AddCopyController();		
		addbookcopy.setStage();		
		addbookcopy.show();
	}
	
	// Event Listener on Button.onAction
	@FXML
	public void handleSignoutAction(MouseEvent event) {
		LoginForm loginForm = new LoginForm();
		loginForm.setStage();
		loginForm.show();
		Stage stage = (Stage) ap.getScene().getWindow();
		stage.hide();
	}
	// Event Listener on Button.onAction
	@FXML
	public void handleMember(ActionEvent event) {
		Member member = new Member();
		member.setStage();
		member.show();
	}
	// Event Listener on Button.onAction
	@FXML
	public void handleBook(ActionEvent event) {
		BookForm bookForm = new BookForm();
		bookForm.setStage();
		bookForm.show();
	}
}
