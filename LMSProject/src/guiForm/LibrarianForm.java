package guiForm;


import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import lms.application.business.Author;
import lms.application.business.Book;
import lms.application.business.LendableCopy;
import lms.application.controller.LibrarianController;
import lms.application.main.Util;


public class LibrarianForm extends Stage implements Initializable{

	
	@FXML
	private TextField memberId;
	
	@FXML
	private TextField bookISBN;
	
	@FXML
	private TableView<Book> CheckoutsTable;
	@FXML
	private Button viewCheckoutsBtn;
	@FXML
	private Button isAvailableBtn;
	@FXML
	private Label isAvailableLbl;
	@FXML
	private Button addItem;		
	@FXML
	private Label UserNameLBL;
	@FXML
	private ComboBox<LendableCopy> bookCopyId;
	
	private List<Book> books = new ArrayList<Book>();
	
	
	public void setStage(){		
		Parent root = null;
		try {
			root = FXMLLoader.load(getClass().getResource("LibrarianForm.fxml"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Scene scene = new Scene(root,600, 631);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		initModality(Modality.WINDOW_MODAL);
		setScene(scene);
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {		
		// TODO Auto-generated method stub	
						
			viewCheckoutsBtn.setOnAction(new EventHandler<ActionEvent>() {
			    @Override
			    public void handle(ActionEvent e) {
			    	getCheckoutRecords();
			    }
			});
			
			isAvailableLbl.setText("Enter Book ISBN to view availability..");
			
			isAvailableBtn.setOnAction(new EventHandler<ActionEvent>() {
			    @Override
			    public void handle(ActionEvent e) {
			    	isBookAvailable();
			    }
			});	
	}
	
	@FXML
    public void handleCheckoutBtn(ActionEvent e) {
    	LibrarianController cntrl = new LibrarianController();
    	cntrl.displayMessage(AlertType.INFORMATION, "Under Construction", "STOP!");
    }
	
	@FXML
    public void handlePrintBtn(ActionEvent e) {
    	LibrarianController cntrl = new LibrarianController();
    	cntrl.print(CheckoutsTable);    	
    }
	@FXML
    public void handleAddItemBtn(ActionEvent e) {
		
		LibrarianController libraryController = new LibrarianController();
		Book book = libraryController.isBookAvailableByISBN(bookISBN.getText());
		if(book != null){
			this.books.add(book);
		}
		
		ObservableList<Book> items = FXCollections.observableArrayList(this.books);
		CheckoutsTable.setItems(items);
		TableColumn<Book,String> title = new TableColumn<>("Copy Id");
		title.setCellValueFactory(new PropertyValueFactory<>("copyId"));

        TableColumn<Book, String> isbn = new TableColumn<>("Book Title");
        isbn.setCellValueFactory(new PropertyValueFactory<>("title"));
        
        TableColumn<Book, String> maxDaysLoan = new TableColumn<>("Due Date");
        maxDaysLoan.setCellValueFactory(new PropertyValueFactory<>("maxDaysLoan"));

        CheckoutsTable.getColumns().addAll(title,isbn,maxDaysLoan);;
        CheckoutsTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        CheckoutsTable.getSelectionModel().setCellSelectionEnabled(false);
    }
	
	public void isBookAvailable(){
		
		LibrarianController cntrl = new LibrarianController();
		if(bookISBN.getText().isEmpty())
		{
			cntrl.displayMessage(AlertType.ERROR, "Enter a valid book ISBN!", "Invalid Input");
			return;
		}
		
		if(cntrl.isBookAvailableByISBN(bookISBN.getText()) != null){
			isAvailableLbl.setText("YES! there is an available copy for checking out");
			addItem.setDisable(false);
			List<LendableCopy> copies = cntrl.getLendableCopyByIsbn(bookISBN.getText());
			ObservableList<LendableCopy> obserList = FXCollections.observableList(copies);   
			bookCopyId.getItems().clear();
			bookCopyId.setItems(obserList);
		}else{
			isAvailableLbl.setText("No copies available for checking out :(");
			addItem.setDisable(true);
		}
	}
	
	
	
	public void getCheckoutRecords(){
		/*LibrarianController cntrl = new LibrarianController();
		String memID = memberId.getText();
		if(memID.isEmpty()){
			cntrl.displayMessage(AlertType.ERROR, "Enter a valid member ID!", "Invalid Input");
			return;
		}
		ObservableList<Book> items = FXCollections.observableArrayList();
		items.addAll(cntrl.getCheckoutsByMemberId(memID));
		CheckoutsTable.setItems(items);	*/
	}
}
	

