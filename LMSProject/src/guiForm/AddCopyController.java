package guiForm;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import lms.application.controller.AdministratorController;

public class AddCopyController extends Stage implements Initializable{
	@FXML
	private TextField isbn;
	@FXML
	private Label availableResult;
	@FXML
	private Button isAvailableBtn;
	@FXML
	private Button addCopyBtn;

	public void setStage(){
		Parent root = null;
		try {
			root = FXMLLoader.load(getClass().getResource("AddCopy.fxml"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Scene scene = new Scene(root,405, 262);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		initModality(Modality.WINDOW_MODAL);
		setScene(scene);
	}
	
	// Event Listener on Button[#isAvailableBtn].onAction
	@FXML
	public void isAvailable(ActionEvent event) {		
		
		AdministratorController adc = new AdministratorController();
		
		if (adc.getBookByISBN(isbn.getText()) != null) {	
		
			availableResult.setText("The book is available...");
			addCopyBtn.visibleProperty().set(true);
			
			
		}else{
					
			addCopyBtn.visibleProperty().set(false);
			availableResult.setText("Sorry, the book is not available...");
			
		}
		
				
	}
	
	
	// Event Listener on Button[#addCopyBtn].onAction	
	@FXML
	public void addCopy(ActionEvent event) {
		
	AdministratorController adc = new AdministratorController();		
				
		if (adc.getBookByISBN(isbn.getText()) != null) {
						
			adc.addBookCopy(isbn.getText());
			
			availableResult.setText("successfully added");
			
		}else{
			
			availableResult.setText("Nothing added...");
			
		}
		
		
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
	}
}
