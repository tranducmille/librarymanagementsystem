package guiForm;

import java.io.IOException;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import lms.application.business.Author;
import lms.application.business.Book;
import lms.application.controller.AdministratorController;

public class BookForm extends Stage{
	@FXML
	private TextField bookTitle;
	@FXML
	private TextField isbn;
	@FXML
	private TableView booksTable;

	public void setStage(){
		Parent root = null;
		try {
			root = FXMLLoader.load(getClass().getResource("Book.fxml"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Scene scene = new Scene(root,576, 431);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		initModality(Modality.WINDOW_MODAL);
		setScene(scene);
	}
	
	// Event Listener on Button.onAction
	@FXML
	public void handleSearchAction(ActionEvent event) {
		AdministratorController adminController = new AdministratorController();
		List<Book> book = adminController.searchBook(bookTitle.getText(), isbn.getText());
		
		
		ObservableList<Book> items = FXCollections.observableArrayList(book);
		booksTable.setItems(items);
		
		ObservableList<TablePosition> selectedCells = FXCollections.observableArrayList();
		TableColumn<Book,String> title = new TableColumn<>("Title");
		title.setCellValueFactory(new PropertyValueFactory<>("title"));

        TableColumn<Book, String> isbn = new TableColumn<>("Isbn");
        isbn.setCellValueFactory(new PropertyValueFactory<>("isbn"));
        
        TableColumn<Book, String> maxDaysLoan = new TableColumn<>("Max Days Loan");
        maxDaysLoan.setCellValueFactory(new PropertyValueFactory<>("maxDaysLoan"));
        
        TableColumn<Book, Boolean> available = new TableColumn<>("Available");
        available.setCellValueFactory(new PropertyValueFactory<>("available"));
        
        booksTable.getColumns().addAll(title,isbn,maxDaysLoan, available);
        booksTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        booksTable.getSelectionModel().setCellSelectionEnabled(false);
	}

}
