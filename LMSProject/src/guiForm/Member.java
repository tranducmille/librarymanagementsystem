package guiForm;

import java.io.IOException;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import lms.application.business.Book;
import lms.application.business.LibraryMember;
import lms.application.controller.AdministratorController;

public class Member  extends Stage{
	@FXML
	private TextField memberId;
	@FXML
	private TextField lastName;
	@FXML
	private TableView membersTable;
	
	public void setStage(){
		Parent root = null;
		try {
			root = FXMLLoader.load(getClass().getResource("Member.fxml"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Scene scene = new Scene(root,576, 431);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		initModality(Modality.WINDOW_MODAL);
		setScene(scene);
	}
	
	// Event Listener on Button.onAction
	@FXML
	public void handleSearchAction(ActionEvent event) {
		AdministratorController adminController = new AdministratorController();
		List<LibraryMember> members = adminController.searchMember(memberId.getText(), lastName.getText());
		
		
		ObservableList<LibraryMember> items = FXCollections.observableArrayList(members);
		membersTable.setItems(items);
		
		ObservableList<TablePosition> selectedCells = FXCollections.observableArrayList();
		TableColumn<Book,String> memberIdCol = new TableColumn<>("Member Id");
		memberIdCol.setCellValueFactory(new PropertyValueFactory<>("memberId"));

        TableColumn<Book, String> firstNameCol = new TableColumn<>("First Name");
        firstNameCol.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        
        TableColumn<Book, String> lastNameCol = new TableColumn<>("Last Name");
        lastNameCol.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        
        TableColumn<Book, Boolean> phoneCol = new TableColumn<>("Phone");
        phoneCol.setCellValueFactory(new PropertyValueFactory<>("phone"));
        
        TableColumn<Book, Boolean> streetCol = new TableColumn<>("Street");
        streetCol.setCellValueFactory(new PropertyValueFactory<>("street"));
        
        TableColumn<Book, Boolean> zipCodeCol = new TableColumn<>("Zip Code");
        zipCodeCol.setCellValueFactory(new PropertyValueFactory<>("zipCode"));
        
        TableColumn<Book, Boolean> stateCol = new TableColumn<>("State");
        stateCol.setCellValueFactory(new PropertyValueFactory<>("state"));
        
        membersTable.getColumns().addAll(memberIdCol,firstNameCol,lastNameCol, phoneCol, streetCol,zipCodeCol ,stateCol);
        membersTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        membersTable.getSelectionModel().setCellSelectionEnabled(false);
	}
}
