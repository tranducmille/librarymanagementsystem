package guiForm;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import lms.application.controller.LoginController;

public class LoginForm extends Stage{
	@FXML
	private PasswordField passwordTxt;
	@FXML
	private TextField userIdTxt;	
	
	@FXML private Pane ap;
		
	public void setStage(){
		Parent root = null;
		
		try {
			root = FXMLLoader.load(getClass().getResource("login.fxml"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Scene scene = new Scene(root,600,280);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		setScene(scene);
		show();
	}
	
	@FXML
	public void handleLoginAction(ActionEvent event) {
		LoginController loginController = new LoginController();
	      String userId = userIdTxt.getText();
	      String password = passwordTxt.getText();
	      boolean rs = loginController.login(userId, password);
	      if(rs){
		      AdminstratorForm adminForm = new AdminstratorForm();
		      Stage stage = (Stage) ap.getScene().getWindow();
		      adminForm.setStage();
		      adminForm.show();
		      stage.hide();
	      }
	}
	@FXML
	public void handleResetAction(ActionEvent event) {
		 userIdTxt.setText("");
	     passwordTxt.setText("");
	}
}
