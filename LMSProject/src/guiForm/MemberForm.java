package guiForm;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collections;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import lms.application.business.LibraryMember;
import lms.application.controller.AdministratorController;
import lms.application.main.Util;

public class MemberForm extends Stage implements Initializable{
	@FXML
	private TextField memberId;
	@FXML
	private TextField firstName;
	@FXML
	private TextField lastName;
	@FXML
	private TextField street;
	@FXML
	private TextField city;
	@FXML
	private TextField zipCode;
	@FXML
	private ComboBox<String> states;
	@FXML
	private TextField phone;
	@FXML
	private Button retrieveMember;
	@FXML
	private Button addBtn;
	@FXML
	private Button updateBtn;

	
	public void setStage(){
		Parent root = null;
		try {
			root = FXMLLoader.load(getClass().getResource("MemberForm.fxml"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Scene scene = new Scene(root,547, 451);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		setScene(scene);
	}
	
	public void addMember(ActionEvent event)
	{
		LibraryMember member=new LibraryMember();
		member.setMemberId(memberId.getText());
		member.setFirstName(firstName.getText());
		member.setLastName(lastName.getText());
		member.setStreet(street.getText());
		member.setCity(city.getText());
		member.setZipCode(zipCode.getText());
		member.setState("Iowa");
		member.setPhone(phone.getText());
		AdministratorController admin=new AdministratorController();
		admin.addMember(member);
	}
	public boolean validatePhone(String phone)
	{
		String regex = "^\\(?([0-9]{3})\\)?[-.\\s]?([0-9]{3})[-.\\s]?([0-9]{4})$";
		Pattern pattern = Pattern.compile(regex);

	    Matcher matcher = pattern.matcher(phone);
	     return matcher.matches();
	}
	public void updateLibraryMember()
	{

		LibraryMember member=new LibraryMember();
		member.setMemberId(memberId.getText());
		member.setFirstName(firstName.getText());
		member.setLastName(lastName.getText());
		member.setStreet(street.getText());
		member.setCity(city.getText());
		member.setZipCode(zipCode.getText());
		member.setState("Iowa");
		member.setPhone(phone.getText());
		AdministratorController admin=new AdministratorController();
		admin.updateLibraryMember(member);
		
	}
	@FXML
	public void getMemberById(ActionEvent event)
	{
		
		AdministratorController admin=new AdministratorController();
		LibraryMember member = admin.getLibraryMemberById(memberId.getText());
		if(member != null){
			firstName.setText(member.getFirstName());
			lastName.setText(member.getLastName());
			street.setText(member.getStreet());
			city.setText(member.getCity());
			zipCode.setText(member.getZipCode());
			//state.setText("Iowa");
			phone.setText(member.getPhone());
			
			addBtn.setDisable(true);
			updateBtn.setDisable(false);
		}		
		
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		ObservableList<String> stateList = FXCollections.observableList(Arrays.asList(Util.stateArray));   
		states.getItems().clear();
		states.setItems(stateList);
	}
}
